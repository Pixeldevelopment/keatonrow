var express = require('express'),
	router = express.Router(),
	http = require('http'),
	helpers = require('../helpers/citycode'),
	appkey = "ec651feeff2d32d20b191cbd574d6be7";

/* GET forecast. */
router.get('/:city?', function (req, res) {
	var uri;
	if(req.params.city !== undefined){
		city_id = helpers.getCityCode(req.params.city);
		if(!city_id) return res.status(404).send('error');
		uri = 'http://api.openweathermap.org/data/2.5/forecast/daily?appid='+appkey+'&cnt=14&id='+city_id+"&units=metric";
	}else{
		uri = 'http://api.openweathermap.org/data/2.5/group?id=5391959,5128581,6077243&appid='+appkey+"&units=metric";
	}

	http.get(uri, function (results) {

		var data = '';
		results.on('data', function(d) {
			data += d;
		});
		results.on('end', function() {
			try {
				return res.json(JSON.parse(data));
			} catch (e) {
				console.error("Got error: "+e.message);
				return res.status(500).send('error');
			}
		});

	}).on('error', function (e) {
		console.error("Got error: "+e.message);
		return res.status(504).send('error');
	});

});



module.exports = router;
