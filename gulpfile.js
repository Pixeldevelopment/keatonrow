var elixir = require('laravel-elixir');

//import vueify

elixir(function(mix) {

	mix.sass([
		'app.scss'
	], 'public/css/main.min.css');

	// Vendor Files
	mix.scripts([
		'../../../node_modules/angular/angular.min.js',
	], 'public/js/vendor.min.js');

	// App Files
	mix.scripts([
		'weatherApp.js',
		'controllers/weatherController.js',
		'directives/weatherDirective.js',
		'services/weatherService.js',
		'filters/titleCase.js',
	], 'public/js/app.min.js');

	mix.browserSync({
		proxy: 'keatonrow.dev'
	});

});
