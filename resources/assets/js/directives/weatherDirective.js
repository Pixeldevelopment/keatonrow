"use strict";

app.directive("weatherAggregation", function(){
	return {
		templateUrl: './views/aggregation.html',
		controller: "weatherCtrl",
		replace: true
	}
});