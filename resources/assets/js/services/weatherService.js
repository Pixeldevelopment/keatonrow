'use strict';

app.service('dataService', function($http, $q){

	this.getForecast = function(callback, errorHandler, city){
		return $http.get('/api/'+city).then(
			callback, errorHandler
		);
	}
	this.getCurrentForecast = function(callback, errorHandler){
		return $http.get('/api/').then(
			callback, errorHandler
		);
	}
	this.forTest = function(){
		setTimeout(function(){console.log('sleep timer!'), 2000});
	}

});
