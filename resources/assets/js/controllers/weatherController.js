'use strict';

app.controller('weatherCtrl', function($scope, dataService, $parse, $q){

	$scope.getDatetime = new Date();
	$scope.loading = true;
	$scope.error = false;
	var cities = ['montreal', 'sanfrancisco', 'newyork'];

	$q.all([
		cities.forEach(function(city){
			dataService.getForecast(function(res){
				$parse(city).assign($scope, res.data);
			}, function(error){
				console.error(error);
				$scope.error = true;
			}, city);
		}),
		dataService.getCurrentForecast(function(res){
			$scope.currentWeather = {};
			res.data.list.forEach(function(item){
				$scope.currentWeather[item.name.replace(/\s+/g, '').toLowerCase()] = item
			});
		}, function(error){
			console.error(error);
			$scope.error = true;
		})

	]).then(function() {
		console.info('Data ready');
		$scope.loading = false;
		$scope.changeCity('montreal');
		$scope.getAverages();
		$scope.getSunnyDays();
	});



	$scope.changeCity = function(city){
		$scope.showAggregation = false;
		$scope.city = $scope.currentWeather[city].name;
		$scope.country = $scope.currentWeather[city].sys.country;
		$scope.currentTemp = $scope.currentWeather[city].main.temp;
		$scope.icon = $scope.currentWeather[city].weather[0].id;
		$scope.description = $scope.currentWeather[city].weather[0].description;
		$scope.humidity = $scope.currentWeather[city].main.humidity;
		$scope.windspeed = $scope.currentWeather[city].wind.speed;
		$scope.days = [];
		for (var i = 1; i <= 3; i++) {
			$scope.days[i] = {
				"date" : ($scope[city].list[i].dt * 1000 ),
				"icon" : $scope[city].list[i].weather[0].id,
				"max" : $scope[city].list[i].temp.max,
				"min" : $scope[city].list[i].temp.min
			}
		}
	}

	$scope.getAverages = function(){
		var city_averages = [];
		cities.forEach(function(city){
			var average = 0;
			$scope[city].list.forEach(function(item, $index){
				average += (item.temp.min + item.temp.max)/2;
			});
			average = Math.round((average / 14) * 1000)/1000;
			city_averages.push({city: $scope[city].city.name, temp: average});
		});
		$scope.lowestAverage = city_averages.reduce(function(prev, curr){
			return prev.temp < curr.temp ? prev : curr;
		});
		$scope.highestAverage = city_averages.reduce(function(prev, curr){
			return prev.temp > curr.temp ? prev : curr;
		});
	}

	$scope.getSunnyDays = function(){
		var sunnyDays = [];
		cities.forEach(function(city){
			var count = 0;
			$scope[city].list.forEach(function(item, $index){
				if(item.weather[0].main === "Clear") count ++;
			});
			sunnyDays.push({city: $scope[city].city.name, count: count});
		});
		$scope.leastSunny = sunnyDays.reduce(function(prev, curr){
			return prev.count < curr.count ? prev : curr;
		});
		$scope.mostSunny = sunnyDays.reduce(function(prev, curr){
			return prev.count > curr.count ? prev : curr;
		});
	}

});


